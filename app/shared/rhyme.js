var db = require("./data.json")
var _ = require("lodash")
var rhymeStructure = [5,4,4,5]


const toReplace = [3, 3]

function rhyme(recipient) {
  let rhyme = {
    list: [],
    string: ''
  }
  let rhymeId = ''
  recipient = recipient || 'Kedvesem'
  for (let [rowNumber, positionNumbers] of rhymeStructure.entries()) {
    rowNumber = rowNumber + 1
    let rowText = ''
    let currentItem = {}
    for (i = 1; i <= positionNumbers ; i++) {
      let currentDb = []
      if (rowNumber%2 === 0 && i === positionNumbers) {
        currentDb = _.filter(db, 
          { row: _.toString(rowNumber), position: _.toString(i), rhymeId: _.toString(rhymeId)})
      } else {
        currentDb = _.filter(db, { row: _.toString(rowNumber), position: _.toString(i) })
      }      
      currentItem = currentDb[_.random(0, currentDb.length - 1)] || {text: 'maci' + rhymeId + 'r' + rowNumber + 'p' + i }
      if (rowNumber%2 !== 0 && i === positionNumbers) rhymeId = currentItem.rhymeId
      rowText = rowText + ' ' + currentItem.text
    }
    if (rhyme.text) rhyme.text = rhyme.text + ',' + `\n` + `\n` + rowText 
    else rhyme.text = rowText
    rhyme.list.push({text: rowText})
  }
  return rhyme

}

module.exports = rhyme;