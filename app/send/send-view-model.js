const observableModule = require("data/observable");

function SendViewModel() {
    const viewModel = observableModule.fromObject({});

    return viewModel;
}

module.exports = SendViewModel;
