/*
In NativeScript, a file with the same name as an XML file is known as
a code-behind file. The code-behind is a great place to place your view
logic, and to set up your page’s data binding.
*/

var HomeViewModel = require("./home-view-model");
var getViewById = require("tns-core-modules/ui/core/view")
var rhyme = new HomeViewModel();
var enums = require("tns-core-modules/ui/enums");
var recipient = "Kedvesem"
var page

function onNavigatingTo(args) {
    page = args.object;
    var tabview = page.getElementById('tabview');
    tabview = page.getElementById('maci')
    console.log('tabviewmac')
    console.log(tabview)
    showNameDialog();
    // tabview._tabLayout.setVisibility(android.view.View.GONE)

    // page.rhyme2 = rhyme.newRhyme().text
    //console.log(page.rhyme)
    page.bindingContext = rhyme
}
exports.newRhyme = function(name) {
    // rhyme.text = rhyme.newRhyme(name).text
    var textField = page.getElementById('rhymetext')
    console.log('textField:')
    console.log(textField)
    var text = rhyme.newRhyme(name).text
    textField.android.setText(android.text.Html.fromHtml(text));
    rhyme.imageNumber = rhyme.getImageNumber()
    rhyme.classNumber = rhyme.getBackgroundNumber()
    
}
exports.orientation = function(args) {
    rhyme.landscape = args.landscape

}

function showNameDialog() {
    // >> dialog-prompt
    const promptOptions = {
        title: "Kinek írod a verset?",
        // message: "Your message",
        okButtonText: "Irj egy szép verset neki",
        // cancelButtonText: "Cancel",
        // neutralButtonText: "Neutral",
        defaultText: recipient,
        inputType: "text", // email, number, text, password, or email
        capitalizationType: "sentences" // all, none, sentences or words
    };
    prompt(promptOptions).then((r) => {
        recipient = r.text
        exports.newRhyme(r.text)
        console.log("Dialog result: ", r.result);
        console.log("Text: ", r.text);
    });
    // << dialog-prompt
}
function showSendDialog() {
    // >> dialog-prompt
    const promptOptions = {
        title: "Sajnos ez még nem működik",
        // message: "Your message",
        okButtonText: "Close",
        // cancelButtonText: "Cancel",
        // neutralButtonText: "Neutral",
        // defaultText: recipient,
        // inputType: "text", // email, number, text, password, or email
        capitalizationType: "sentences" // all, none, sentences or words
    };
    
    prompt(promptOptions).then((r) => {
        //recipient = r.text
        //exports.newRhyme(r.text)
        console.log("Dialog result: ", r.result);
        console.log("Text: ", r.text);
    });

    // << dialog-prompt
}

exports.showSendDialog = showSendDialog;
exports.showNameDialog = showNameDialog;

exports.onNavigatingTo = onNavigatingTo;
