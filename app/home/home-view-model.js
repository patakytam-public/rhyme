const observableModule = require("tns-core-modules/data/observable");
var ObservableArray = require("tns-core-modules/data/observable-array").ObservableArray;
var db = require("../shared/data.json")
var _ = require("lodash")
var numberOfImages = 11
var numberOfBackgrounds = 8

function HomeViewModel() {
    var viewModel = observableModule.fromObject({
        text: 'cica',
        title: 'Rímfaragó',
        landscape: false,
        classNumber: 1,
        imageNumber: 1,        
        groceryList: new ObservableArray([
            { name: "Apples" },
            { name: "Bananas" },
            { name: "Oranges" }
        ])
    });

    var rhymeStructure = [5,4,4,5]
    const toReplace = [3, 3]
    viewModel.getImageNumber = function() {
       return _.random(1, numberOfImages) 
    }
    viewModel.getBackgroundNumber = function() {
        return _.random(1, numberOfBackgrounds) 
    }  
    viewModel.newRhyme = function(recipient = 'Kedvesem') {
        let rhyme = {
            list: [],
            string: ''
        }
        let rhymeId = ''
        for (let [rowNumber, positionNumbers] of rhymeStructure.entries()) {
            rowNumber = rowNumber + 1
            let rowText = ''
            let currentItem = {}
            for (i = 1; i <= positionNumbers ; i++) {
            let currentDb = []
            if (rowNumber%2 === 0 && i === positionNumbers) {
                currentDb = _.filter(db, 
                { row: _.toString(rowNumber), position: _.toString(i), rhymeId: _.toString(rhymeId)})
            } else {
                currentDb = _.filter(db, { row: _.toString(rowNumber), position: _.toString(i) })
            }      
            currentItem = currentDb[_.random(0, currentDb.length - 1)] || {text: 'maci' + rhymeId + 'r' + rowNumber + 'p' + i }
            if (rowNumber%2 !== 0 && i === positionNumbers) rhymeId = currentItem.rhymeId
            if (rowNumber === toReplace[0] && i === toReplace[1]) {
               currentItem.text = `<font size="58" color="yellow"><b>${recipient}</b></font>,` 
            }
            
            rowText = rowText + ' ' + currentItem.text
            }
            if (rhyme.text) rhyme.text = rhyme.text + ',' + `<br>` + `<br>` + rowText
            else rhyme.text = rowText
            rhyme.list.push({text: rowText})
        }
        rhyme.text = rhyme.text + '.' + `<br>` 
        // rhyme.text = textField.android.setText(android.text.Html.fromHtml(rhyme.text))
        return rhyme
    }
    viewModel.text = viewModel.newRhyme().text
    return viewModel;
}

module.exports = HomeViewModel;
