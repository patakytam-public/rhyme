
# Szerelmes vers generátor (WIP)
Írj szerelmes verset bárkinek! Szinte végtelen variációs lehetőség!

## Tecnologies
[Nativescript](https://www.nativescript.org) , vanilla js

## Demo
Download Nativescript Playground and Nativescript Preview app to your phone:

**NativeScript Playground** - used to scan the QR codes:
- [App Store (iOS)](https://apps.apple.com/us/app/nativescript-playground/id1263543946)
-  [Google Play (Android)](https://play.google.com/store/apps/details?id=org.nativescript.play)

**NativeScript Preview** - used to display your app:
-  [App Store (iOS)](https://apps.apple.com/us/app/nativescript-preview/id1264484702)
-  [Google Play (Android)](https://play.google.com/store/apps/details?id=org.nativescript.preview)

Start **NativeScript Playground** on your app and scan the QR code below:

![Demo](./app/assets/demo/app_qr.png)

Enjoy!

## Image gallery
![Image 1.](./app/assets/demo/demo1.jpg)
![Image 2.](./app/assets/demo/demo2.jpg)
![Image 3.](./app/assets/demo/demo3.jpg)
![Image 4.](./app/assets/demo/demo4.jpg)